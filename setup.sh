#!/bin/bash

function escribeVirtualHost()
{
  echo $1 >> ./000-default.conf
}

function escribeAmarillo()
{
  echo -e "\e[1;33m$1\e[0m"
}
function escribeVerde()
{
  echo -e "\e[1;32m$1\e[0m"
}
function escribeRojo()
{
  echo -e "\e[1;31m$1\e[0m"
}

function ask()
{
  while true; do
    escribeAmarillo "$1"
    read -p "[y/n] " yn

    case $yn in
      [yY] ) echo "";
          escribeVerde "Ok, procedemos con la instalación";
          break;;
      [nN] ) break;;
      * ) escribeRojo "Respuesta inválida";;
    esac
  done
}


escribeAmarillo "Archivista v1.0"
escribeAmarillo "Proceso de instalación de dependencias"
echo ""

cp .env.production .env

ask "¿Necesitas instalar un servidor web?"
case $yn in
  [yY] ) echo "";
         escribeVerde "Instalando Apache";
         sudo apt-get install apache2;
         sudo a2enmod rewrite;
         touch ./000-default.conf;
         escribeVirtualHost "<VirtualHost *:80>";
         escribeVirtualHost " ServerName localhost";
         escribeVirtualHost " DocumentRoot $(pwd)/public";
         escribeVirtualHost "";
         escribeVirtualHost " <Directory $(pwd)>";
         escribeVirtualHost "   Options Indexes FollowSymLinks MultiViews";
         escribeVirtualHost "   AllowOverride All";
         escribeVirtualHost "   Order allow,deny";
         escribeVirtualHost "   Allow from all";
         escribeVirtualHost "   Require all granted";
         escribeVirtualHost " </Directory>";
         escribeVirtualHost "";
         escribeVirtualHost " ErrorLog ${APACHE_LOG_DIR}/error.log";
         escribeVirtualHost " CustomLog ${APACHE_LOG_DIR}/access.log combined";
         escribeVirtualHost "</VirtualHost>";
         sudo mv ./000-default.conf /etc/apache2/sites-available/000-default.conf;
         sudo systemctl restart apache2;;

  [nN] ) echo "";
         escribeAmarillo "Recuerda habilitar el VirtualHost";;
esac

echo ""
ask "¿Necesitas instalar una base de datos?"
case $yn in
  [yY] ) echo "";
      echo "Elige servidor:"
      select yn in "MySQL" "MariaDB"; do
        case $yn in
          MySQL ) echo "";
                escribeVerde "Instalando MySQL";
                sudo apt-get install mysql-server;
                sudo mysql_secure_installation;
                break;;
          MariaDB ) echo "";
              escribeVerde "Instalando MariaDB";
              sudo apt-get install mariadb-server;
              sudo mysql_secure_installation;
              break;;
        esac
      done;;
esac

echo ""
ask "¿Quieres crear la base de datos? (Si no lo haces ahora, tendrás que hacerlo manualmente)"
case $yn in
  [yY] ) echo "";
      while true; do
        read -p "Introduce el nombre de la base de datos: " nombre
        read -p "¿Es el nombre que deseas darle? [y/n]: " conf

        case $conf in
          [yY] ) sudo mysql -e "CREATE DATABASE $nombre";
              escribeAmarillo "Base de datos creada con el nombre $nombre";
              break;;
        esac
      done
esac

echo ""
ask "¿Necesitas instalar PHP?"
case $yn in
  [yY] ) echo "";
        escribeVerde "Instalando PHP";
        sudo apt-get install libapache2-mod-php php php-common php-xml php-mysql php-gd php-opcache php-mbstring php-tokenizer php-json php-bcmath php-zip unzip -y;
        escribeAmarillo "IMPORTANTE";
        escribeAmarillo "Después de esta instalación tienes que configurar el fichero php.ini";
        escribeAmarillo "Está situado en la siguiente ruta:";
        escribeVerde "     /etc/php/[La versión de PHP]/apache2/php.ini";
        echo ""
        escribeAmarillo "Debes buscar la línea en la que ponga ';cgi.fix_pathinfo=1' y sustituírla por 'cgi.fix_pathinfo=0'";
        escribeAmarillo "También debes buscar la línea en la que ponga 'short_open_tag=Off' y sustituírla por 'short_open_tag=On'";;
esac

echo ""
ask "¿Necesitas instalar PhpMyAdmin?"
case $yn in
  [yY] ) echo "";
        escribeVerde "Instalando PhpMyAdmin";
        sudo apt-get install phpmyadmin;;
esac

echo ""
ask "¿Necesitas instalar Composer?"
case $yn in
  [yY] ) echo "";
        escribeVerde "Instalando Composer";
        sudo apt-get install curl;
        curl -sS https://getcomposer.org/installer | php;
        sudo mv composer.phar /usr/local/bin/composer;
        composer --version
esac

escribeAmarillo "Instalando dependencias"
composer install --no-dev

echo ""
escribeAmarillo "Asignando permisos"

sudo chown www-data:www-data * -Rf
sudo chown www-data:www-data .env
sudo chown www-data:www-data .editorconfig
sudo chown www-data:www-data .styleci.yml
