<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertSomeData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     private $tipo = ["Antología","Novela", "Relato"];
     private $genres = ["Arquitectura", "Arte", "Aventuras", "Biografía", "Ciencia", "Ciencia ficción", "Cine", "Cocina", "Comedia", "Cómic", "Cuento", "Deporte", "Diccionario", "Enciclopedia", "Ensayo y Filosofía", "Erótica", "Fábula", "Fantasía", "Geografía", "Guía", "Historia", "Histórica", "Humanidades", "Ilustraciones", "Informática", "Juvenil", "Lengua y Literatura", "Manualidades", "Matemáticas", "Medicina", "Mitología", "Naturaleza", "Negra", "No ficción", "Poesía", "Política", "Reportajes", "Romántica", "Suspense", "Teatro", "Terror", "Viajes y reportajes"];
     private $formatos = ["Bolsillo", "Caja protectora", "Con forro", "Con solapas", "Digital", "Encuadernación japonesa", "Espiral", "Grapa", "Tapa blanda", "Tapa dura"];

    public function up()
    {
      foreach($this->tipo as $t)
      {
        $id = DB::table('genres')->insertGetId(['name' => $t]);
        DB::table('tipo_obras')->insert(['genre_id' => $id]);
      }

      foreach($this->genres as $g)
      {
        DB::table('genres')->insert(['name' => $g]);
      }

      foreach($this->formatos as $f)
      {
        DB::table('tipos')->insert(['name' => $f]);
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::table('genres')->delete();
      DB::table('tipo_obras')->delete();
      DB::table('tipos')->delete();
    }
}
