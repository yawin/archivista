<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('descript')->nullable();
            $table->string('editorial');
            $table->string('isbn');
            $table->string('date');
            $table->integer('pagecount');
            $table->timestamps();
        });

        Schema::create('bookauthors', function (Blueprint $table) {
          $table->id();
          $table->integer('bookid');
          $table->integer('authorid');
          $table->timestamps();
        });

        Schema::create('bookgenres', function (Blueprint $table) {
          $table->id();
          $table->integer('bookid');
          $table->integer('genreid');
          $table->timestamps();
        });

        Schema::create('booktypes', function (Blueprint $table) {
          $table->id();
          $table->integer('bookid');
          $table->integer('typeid');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
        Schema::dropIfExists('bookauthors');
        Schema::dropIfExists('bookgenres');
        Schema::dropIfExists('booktypes');
    }
}
