# Archivista <small>v1.0 - OpenBeta</small>

Archivista es una aplicación web para catalogar bibliotecas personales desarrollada con el framework Laravel.

## Instalando Archivista
Descomprime la aplicación en la carpeta que consideres correcta y ejecuta el script `setup.sh`. Después, accede desde el navegador a la aplicación y rellena el formulario de setup.

<center>

[<img alt="Tutorial de instalación" src="https://home.yawin.es/img/inst.png" width="250px"/>](https://video.hardlimit.com/w/cz6p8H1YQ98f5fiXcUGPM9)

</center>


### Resolviendo algunos problemas
#### Configurando PHP
Según la versión de PHP, en la configuración hay dos cambios que se deben realizar:
  * Descomentar y poner a 0 `cgi.fix_pathinfo`
  * Activar las `short_open_tag`

#### Configurando el VirtualHost
En la carpeta `/etc/apache2/sites-available` se debe crear un fichero `.conf`. Por ejemplo, `archivista.conf`. En él se debe introducir el siguiente contenido.
    ServerName localhost
    <VirtualHost *:80>
      DocumentRoot [Ruta a la carpeta de Archivista]/public

      <Directory [Ruta a la carpeta de Archivista]>
        Options Indexes FollowSymLinks MultiViews
        AllowOverride All
        Order allow,deny
        Allow from all
        Require all granted
      </Directory>

      ErrorLog ${APACHE_LOG_DIR}/error.log
      CustomLog ${APACHE_LOG_DIR}/access.log combined
    </VirtualHost>

Una vez creado ese archivo se deben ejecutar los siguientes comandos:

    sudo a2ensite [nombre del fichero]
    sudo systemctl restart apache2
