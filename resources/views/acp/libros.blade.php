@extends('layouts.app')

@section('content')
<script src="{{ asset('js/books.js') }}" defer></script>
<div class="container-fluid">
  <table class="table table-hover">
    <thead>
      <tr class="table-borderless">
        <th style="width: 250px;">Título</th>
        <th style="width: 175px;">Fecha de edición</th>
        <th style="text-align: center; width: 80px;">Pág.</th>
        <th>Editorial</th>
        <th style="width: 150px;">Autores</th>
        <th>&nbsp;</th>
        <th style="width: 150px;">Géneros</th>
        <th>&nbsp;</th>
        <th style="width: 150px;">Formato</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
      </tr>
    </thead>
    <tbody>
      @foreach(App\Models\Book::getAll() as $book)
        <tr id="book_{{$book->id}}">
          <td><input OnChange="updateField({{$book->id}}, 'title');" id="title_{{$book->id}}" class="form-control" value="{{$book->title}}"/></td>
          <td><input OnChange="updateField({{$book->id}}, 'date');" id="date_{{$book->id}}" class="form-control" value="{{$book->date}}"/></td>
          <td><input OnChange="updateField({{$book->id}}, 'pagecount');" id="pagecount_{{$book->id}}" class="form-control" value="{{$book->pagecount}}"/></td>
          <td>
            <select class="form-control">
                <option value="-1" disabled>Selecciona editorial</option>
                <option value="-1" disabled></option>
                @foreach(App\Models\Editorial::getAll() as $editorial)
                  @if($editorial->id != $book->editorial)
                    <option OnClick="updateEditorial({{$book->id}}, {{$editorial->id}})" value="{{$editorial->id}}">{{$editorial->name}}</option>
                  @else
                    <option selected value="{{$editorial->id}}">{{$editorial->name}}</option>
                  @endif
                @endforeach
             </select>
           </td>
          <td>
            <select class="form-control">
                <option value="-1" disabled>Más autores</option>
                <option value="-1" disabled></option>
                @php
                  $aut = App\Models\Autor::join("bookauthors", "autors.id", "=", "bookauthors.authorid")->select("autors.id")->where("bookauthors.bookid", "=", $book->id)->get()->toArray();
                  $autores = App\Models\Autor::whereNotIn("id", $aut)->orderBy("name")->get();
                @endphp
                @foreach($autores as $autor)
                  <option OnClick="addauth({{$book->id}},{{$autor->id}})" value="{{$autor->id}}">{{$autor->name}}</option>
                @endforeach
             </select>
          </td>
          <td>
            @foreach($book->getAuthors() as $autor)
              <span class="badge badge-secondary">{{$autor[1]}} <i OnClick="delauth({{$book->id}},{{$autor[0]}})" class="fa fa-times"></i></span>&nbsp;
            @endforeach
          </td>
          <td>
            <select class="form-control">
                <option value="-1" disabled>Más géneros</option>
                <option value="-1" disabled></option>
                @php
                  $gen = App\Models\Genre::join("bookgenres", "genres.id", "=", "bookgenres.genreid")->select("genres.id")->where("bookgenres.bookid", "=", $book->id)->get()->toArray();
                  $genres = App\Models\Genre::whereNotIn("id", $gen)->orderBy("name")->get();
                @endphp
                @foreach($genres as $genre)
                  <option OnClick="addgen({{$book->id}},{{$genre->id}})" value="{{$genre->id}}">{{$genre->name}}</option>
                @endforeach
             </select>
          </td>
          <td>
            @foreach($book->getGenres() as $genre)
              <span class="badge badge-secondary">{{$genre[1]}} <i OnClick="delgen({{$book->id}},{{$genre[0]}})" class="fa fa-times"></i></span>&nbsp;
            @endforeach
          </td>
          <td>
            <select class="form-control">
                <option value="-1" disabled>Más géneros</option>
                <option value="-1" disabled></option>
                @php
                  $tip = App\Models\Tipo::join("booktypes", "tipos.id", "=", "booktypes.typeid")->select("tipos.id")->where("booktypes.bookid", "=", $book->id)->get()->toArray();
                  $tipos = App\Models\Tipo::whereNotIn("id", $tip)->orderBy("name")->get();
                @endphp
                @foreach($tipos as $tipo)
                  <option OnClick="addtype({{$book->id}},{{$tipo->id}})" value="{{$tipo->id}}">{{$tipo->name}}</option>
                @endforeach
             </select>
          </td>
          <td>
            @foreach($book->getTypes() as $tipo)
              <span class="badge badge-secondary">{{$tipo[1]}} <i OnClick="deltype({{$book->id}},{{$tipo[0]}})" class="fa fa-times"></i></span>&nbsp;
            @endforeach
          </td>
          <td><a class="btn btn-secondary text-white" href="{{ route('acp.editLibro', $book->id) }}"><span class="far fa-file"></span></a></td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
@endsection
