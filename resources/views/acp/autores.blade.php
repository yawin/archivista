@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-header bg-primary text-white">
            Autores
            <span id="verunificar" class="float-right">
              <button id="unificabutton" title="Unificar" OnClick="toggleUnificador();" type="button" class="btn btn-sm btn-outline-light"><span class="fas fa-magnet"></span></button>
            </span>
            <span hidden id="ocultarunificar" class="float-right">
              <a id="unificabutton" title="Ocultar" OnClick="toggleUnificador();" type="button" class="btn btn-sm btn-outline-light"><span class="fas fa-eye-slash"></span></a>
              <a title="Unificar" OnClick="applyunify();" type="button" class="btn btn-sm btn-success text-white"><span class="fas fa-save"></span></a>
            </span>
        </div>
        <div class="card-body">
          <table class="table table-hover">
            <tbody>
              @php
                $generodeautor = [-1 => 0, 0 => 0, 1 => 0, 2 => 0]
              @endphp
              @foreach(App\Models\Autor::getAll() as $a)
                <tr>
                  <td id="a_{{$a->id}}" OnClick="muestra('{{$a->id}}');" class="align-middle">{{ $a->name }}</td>
                  <td id="g_{{$a->id}}" OnClick="muestra('{{$a->id}}');" class="align-middle">
                    {{$a->getGenre()}}
                    @php
                      $generodeautor[$a->genre] = $generodeautor[$a->genre]+1;
                    @endphp
                  </td>
                  <td id="p_{{$a->id}}" OnClick="muestra('{{$a->id}}');" class="align-middle">@if(!empty($a->country)){{$paises[$a->country]}}@endif</td>
                  <td id="b_{{$a->id}}" style="width:25px;" class="align-middle"></td>
                  <td hidden style="width:50px;" class="checkhidden align-middle"><input id="{{$a->id}}" class="form-control" type="checkbox" /></td>
                </tr>
              @endforeach
              <tr hidden ><td><span id="gen_">{{$generodeautor[-1]}}</span><span id="gen_0">{{$generodeautor[0]}}</span><span id="gen_1">{{$generodeautor[1]}}</span><span id="gen_2">{{$generodeautor[2]}}</span></td><td></td><td></td></tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="card">
        <div class="card-header bg-primary text-white">Nuevo autor</div>
        <div class="card-body">
          <form id="editor" method="POST" action="{{ route('acp.putautores') }}">
            @csrf
            @method('PUT')
            <input id="autor_id" name="autor_id" hidden value="-1"/>
            <div class="form-group">
              <input id="autor_name" name="name" class="form-control" placeholder="Nombre y apellidos"/>
            </div>
            <div class="form-group">
              <select id="autor_genre" name="genre" class="form-control">
                <option value="-1">Género sin asignar</option>
                <option value="0">Señor</option>
                <option value="1">Señora</option>
                <option value="2">Señore</option>
              </select>
            </div>
            <div class="form-group">
              <select id="autor_country" name="country" class="form-control">
                <option id="empty" value="">País sin asignar</option>
                @foreach ($paises as $country_id => $country)
                  <option id="{{ $country }}" value="{{ $country_id }}">{{ $country }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <input type="submit" class="btn btn-outline-primary form-control" value="Guardar"/>
            </div>
          </form>
        </div>
      </div>
      <br/>
      <div class="alert alert-info" role="alert">
        <p>
          Hay un total de {{$generodeautor[-1]+$generodeautor[0]+$generodeautor[1]+$generodeautor[2]}} autores: {{$generodeautor[0]}}&#9794;, {{$generodeautor[1]}}&#9792; y {{$generodeautor[2]}}nb
        </p>
      </div>
      <script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
      <div id="chartContainer" style="height: 300px; width: 100%;"></div>
    </div>
  </div>
</div>

<div id="hidden" hidden>
  <input id="edit_name" name="edit_name" class="form-control" type="text"/>
  <select id="edit_genre" name="genre" class="form-control">
    <option value="-1">Sin asignar</option>
    <option id="Señor" value="0">Señor</option>
    <option id="Señora" value="1">Señora</option>
    <option id="Señore" value="2">Señore</option>
  </select>
  <select id="edit_country" name="country" class="form-control">
    <option id="empty" value="">Sin asignar</option>
    @foreach ($paises as $country_id => $country)
      <option id="{{ str_replace(" ", "_", $country) }}" value="{{ $country_id }}">{{ $country }}</option>
    @endforeach
  </select>
  <button id="edit_boton" class="btn btn-primary"><span class="far fa-save"></span></button>
  <form id="unify-form" method="POST" action="{{ route('acp.unificautores') }}">
    @csrf
    @method('PUT')
    <input id="unify-input" name="unify-input" class="form-control" type="text"/>
  </form>
</div>
<script>
  function muestra(aid)
  {
    value = $("#a_"+aid).html();
    $("#a_"+aid).prop("onclick", null).off("click");

    editor =  $("#hidden").find("#edit_name").clone();
    editor.val(value.trim());
    $("#a_"+aid).html(editor);

    value = $("#g_"+aid).html();
    $("#g_"+aid).prop("onclick", null).off("click");

    editor = $("#hidden").find("#edit_genre").clone();
    editor.find('#'+value.trim()).attr("selected", true);
    $("#g_"+aid).html(editor);

    value = $("#p_"+aid).html();
    $("#p_"+aid).prop("onclick", null).off("click");
    if(!value){value = "empty";}

    editor = $("#hidden").find("#edit_country").clone();
    editor.find('#'+value.replace(/ /g, "_").trim()).attr("selected", true);
    $("#p_"+aid).html(editor);

    editor = $("#hidden").find("#edit_boton").clone();
    editor.on("click", function(){ guarda(aid); });
    $("#b_"+aid).html(editor);
  }

  function guarda(aid)
  {
    name = $("#a_"+aid).find("#edit_name").val();
    genre = $("#g_"+aid).find("#edit_genre option:selected").val();
    country = $("#p_"+aid).find("#edit_country option:selected").val();

    $("#autor_id").val(aid);
    $("#autor_name").val(name);
    $("#autor_genre").val(genre).change();
    $("#autor_country").val(country).change();
    $("#editor").submit();
  }

  var ver = false;
  function toggleUnificador()
  {
    if(!ver)
    {
      $("table").find(".checkhidden").map(function() { return $(this).removeAttr("hidden"); });
      $("#verunificar").attr("hidden", "hidden");
      $("#ocultarunificar").removeAttr("hidden");
      ver = true;
    }
    else
    {
      $("table").find(".checkhidden").map(function() { return $(this).attr("hidden", "hidden"); });
      $("#verunificar").removeAttr("hidden");
      $("#ocultarunificar").attr("hidden", "hidden");
      ver = false;
    }
  }

  function applyunify()
  {
    var checks = $('input[type=checkbox]:checked').map(function(){
          return this.id;
      }).get().join(",");

    $("#unify-input").val(checks);
    $("#unify-form").submit();
  }

  function grafico()
  {
    s1 = $("#gen_0").html();
    s2 = $("#gen_1").html();
    s3 = $("#gen_2").html();
    s4 = $("#gen_").html();

    var chart = new CanvasJS.Chart("chartContainer",
    {
      legend: {
        maxWidth: 350,
        itemWidth: 120
      },
      data: [
        {
          type: "pie",
          showInLegend: false,
          dataPoints: [
            { y: s1, indexLabel: "Señor ("+s1+")" },
            { y: s2, indexLabel: "Señora ("+s2+")" },
            { y: s3, indexLabel: "Señore ("+s3+")" },
            { y: s4, indexLabel: "Sin asignar ("+s4+")" }
          ]
        }
      ]
    });
    chart.render();
  }

  $(document).ready(
    function()
    {
      grafico();
    });
</script>
@endsection
