@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="card" style="width: 100%">
      <div class="card-header bg-primary text-white">Nuevo libro</div>
      <div class="card-body">
        <script>
          function busca_isbn_google()
          {
            isbn_input = $("#isbn_input").val();
            motor = $("#motor").val();
            $.get("{{ route('api.isbn',"") }}"+"/"+isbn_input+"/"+motor, function(output) // "callback":"handleResponse",
            {
              data = JSON.parse(output);
              if(data["error"])
              {
                alert(data["error"]);
                return;
              }

              $("#title").val(data["title"]);
              $("#date").val(data["date"]);
              $("#isbn").val(data["isbn"]);
              $("#pagecount").val(data["pagecount"]);
              $("#descript").html(data["descript"]);
              $("#author").val(data["author"]);
            });
          }
        </script>
        <form id="save-form" method="POST" action="{{route('acp.newbook')}}" enctype="multipart/form-data">
          @method('PUT')
          @csrf
          <div class="row">
            <div class="col-md-6">
              <span class="btn btn-sm btn-outline-primary" OnClick="busca_isbn_google();">Buscar</span>
              <input id="isbn_input" type="text"/>
              <select id='motor'>
                @foreach(Archivista\ApiCall::$methods as $m)
                  <option value='{{$m}}'>{{$m}}</option>
                @endforeach
              </select>
            </div>
            <div class="col-md-6 text-right">
              <button type="submit" class="btn btn-primary">
                Insertar
              </button>
            </div>
          </div>
          <hr/>
          <div class="row justify-content-center">
            <div class="col-md-8">
              <div class="row">
                <div class="col-md-6">
                    {!! App\Models\UIAssistant::getInputField("Título", "title", old("title")) !!}
                    {!! App\Models\UIAssistant::getInputField("Autoría", "author", old("author")) !!}
                    {!! App\Models\UIAssistant::getTextField("Descripción", "descript", old("descript"), "100%", "100%", "") !!}
                </div>
                <div class="col-md-6">
                  {!! App\Models\UIAssistant::getSelectField("Editorial", "editorial", App\Models\UIAssistant::formatArray(App\Models\Editorial::getAll(), ['id', 'name'])) !!}
                  {!! App\Models\UIAssistant::getInputField("ISBN", "isbn", old("isbn")) !!}
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="col-md-6">
                  {!! App\Models\UIAssistant::getInputField("Fecha de publicación", "date", old("date")) !!}
                  {!! App\Models\UIAssistant::getInputField("Páginas", "pagecount", old("pagecount")) !!}
                </div>
                <div class="col-md-6">
                {!! App\Models\UIAssistant::getMultipleSelectField("Formato", "tipo", App\Models\UIAssistant::formatArray(App\Models\Tipo::getAll(), ['id', 'name'])) !!}
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="row">
                {!! App\Models\UIAssistant::getMultipleSelectField("Géneros", "genero", App\Models\UIAssistant::formatArray(App\Models\Genre::getAll(), ['id', 'name']), "352px") !!}
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
