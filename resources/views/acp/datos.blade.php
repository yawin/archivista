@extends('layouts.app')

@section('content')
<div hidden>
  <form id="editor" method="POST" action="{{ route('acp.putdatos') }}">
    @csrf
    @method('PUT')
    <div class="input-group mb-3">
      <input id="edit_id" name="edit_id" hidden value="-1"/>
      <input id="edit_field" name="edit_field" hidden value=""/>
      <input id="edit_name" name="edit_name" class="form-control" type="text"/>
      <div class="input-group-append">
        <button class="btn btn-primary" type="submit"><span class="far fa-save"></span></button>
      </div>
    </div>
  </form>
</div>
<script>
  function muestra(fieldid, eid = "-1")
  {
    value = "";
    if(fieldid.length > 2)
    {
      value = $("#"+fieldid).html();
      $("#"+fieldid).prop("onclick", null).off("click");
    }
    else
    {
      $("#"+fieldid+"tr").removeAttr("hidden");
    }

    editor = $("#editor").clone();
    editor.find('#edit_id').val(eid);
    editor.find('#edit_field').val(fieldid[0]);
    editor.find('#edit_name').val(value);

    $("#"+fieldid).html(editor);
  }

  function muestraTipoObra()
  {
    $("#tipo_tr").removeAttr("hidden");
  }

  function eliminaTipoObra(id)
  {
    $("#to_edit_id").val(id);
    $("#to_editor").submit();
  }
</script>
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-3">
            <div class="card">
                <div class="card-header bg-primary text-white">
                  Editoriales
                  <span class="float-right">
                    <button OnClick="muestra('e_');" class="btn btn-sm btn-outline-light">+</button>
                  </span>
                </div>
                <div class="card-body">
                  <table class="table table-hover">
                    <tbody>
                      <tr id="e_tr" hidden>
                        <td id="e_" class="align-middle"></td>
                      </tr>
                      @foreach(App\Models\Editorial::getAll() as $e)
                        <tr>
                          <td id="e_{{$e->id}}" OnClick="muestra('e_{{$e->id}}', '{{$e->id}}');" class="align-middle">{{ $e->name }}</td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
        <div class="col-md-3">
          <div class="card">
            <div class="card-header bg-primary text-white">
              Formato
              <span class="float-right">
                <button OnClick="muestra('t_');" class="btn btn-sm btn-outline-light">+</button>
              </span>
            </div>
            <div class="card-body">
              <table class="table table-hover">
                <tbody>
                  <tr id="t_tr" hidden>
                    <td id="t_" class="align-middle"></td>
                  </tr>
                  @foreach(App\Models\Tipo::getAll() as $t)
                    <tr>
                      <td id="t_{{$t->id}}" OnClick="muestra('t_{{$t->id}}', '{{$t->id}}');" class="align-middle">{{ $t->name }}</td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="card">
            <div class="card-header bg-primary text-white">
              Géneros
              <span class="float-right">
                <button OnClick="muestra('g_');" class="btn btn-sm btn-outline-light">+</button>
              </span>
            </div>
            <div class="card-body">
              <table class="table table-hover">
                <tbody>
                  <tr id="g_tr" hidden>
                    <td id="g_" class="align-middle"></td>
                  </tr>
                  @foreach(App\Models\Genre::getAll() as $g)
                    <tr>
                      <td id="g_{{$g->id}}" OnClick="muestra('g_{{$g->id}}', '{{$g->id}}');" class="align-middle">{{ $g->name }}</td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="card">
            <div class="card-header bg-primary text-white">
              Tipo de obra
              <span class="float-right">
                <button OnClick="muestraTipoObra();" class="btn btn-sm btn-outline-light">+</button>
              </span>
            </div>
            <div class="card-body">
              <form id="to_editor" method="POST" action="{{ route('acp.putdatos') }}">
                <table class="table table-hover">
                  <tbody>
                    <tr id="tipo_tr" hidden>
                      <td class="align-middle">
                        @csrf
                        @method('PUT')
                        <input id="to_edit_id" name="edit_id" hidden value="-1"/>
                        <input id="edit_field" name="edit_field" hidden value="tipo_obra"/>
                        {!! App\Models\UIAssistant::getSelectField("", "tipo", App\Models\UIAssistant::formatArray(App\Models\TipoObra::getCandidates(), ['id', 'name'])) !!}
                      </td>
                      <td style="width:25px;" class="align-middle"><button class="btn btn-primary" type="submit"><span class="far fa-save"></span></button></td>
                    </tr>
                    @foreach(App\Models\TipoObra::getAll() as $t)
                    <tr>
                      <td id="tipo_{{$t->id}}" class="align-middle">{{ $t->name }}</td>
                      <td style="width:25px;" class="align-middle"><button OnClick="eliminaTipoObra({{ $t->id }});" class="btn btn-sm btn-danger" type="button" title="Eliminar"><span class="fas fa-times"></span></button></td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </form>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
