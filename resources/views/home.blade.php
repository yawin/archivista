@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <table class="table table-hover">
            <thead>
              <tr class="table-borderless">
                <th>Título</th>
                <th>Autor</th>
                <th>Editorial</th>
                <th>Géneros</th>
                <th>Páginas</th>
                <th>Formato</th>
              </tr>
            </thead>
            <tbody>
              @php
                $generodeautor = [-1 => 0, 0 => 0, 1 => 0, 2 => 0]
              @endphp
              @if(count($books) > 0)
                @foreach($books as $b)
                <tr>
                  <td>{{ $b->title }}</td>
                  <td>
                    @foreach($b->getAuthors() as $a)
                      <a href="{{ route('filtered',['author',$a[1]])}}">{{$a[1]}}</a>@if(!$loop->last),@endif
                      @php
                        $generodeautor[$a[2]] = $generodeautor[$a[2]]+1
                      @endphp
                    @endforeach
                  </td>
                  <td><a href="{{ route('filtered',['editorial',App\Models\Editorial::getName($b->editorial)])}}">{{ App\Models\Editorial::getName($b->editorial) }}</a></td>
                  <td>
                    @foreach($b->getGenres() as $g)
                      <a href="{{ route('filtered',['genre',$g[1]])}}">{{$g[1]}}</a>@if(!$loop->last),@endif
                    @endforeach
                  </td>
                  <td>{{ $b->pagecount }}</td>
                  <td>
                    @foreach($b->getTypes() as $t)
                      <a href="{{ route('filtered',['type', $t[1]])}}">{{$t[1]}}</a>@if(!$loop->last),@endif
                    @endforeach
                  </td>
                </tr>
                @endforeach
              @else
                <tr><td>No hay libros registrados</td><td></td><td></td><td></td><td></td><td></td></tr>
              @endif
              <tr hidden ><td></td><td id="gen_">{{$generodeautor[-1]}}</td><td id="gen_0">{{$generodeautor[0]}}</td><td id="gen_1">{{$generodeautor[1]}}</td><td id="gen_2">{{$generodeautor[2]}}</td><td></td></tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="card">
        <div class="card-header bg-primary text-white">Búsqueda</div>
        <div class="card-body">
          <div class="form-group">
              <input id="textsearch" class="form-control" placeholder="Título, autor, editorial o género"/>
          </div>
          <div class="form-group">
            <select id="searchtype" class="form-control">
              <option value="title">Título</option>
              <option value="author">Autor</option>
              <option value="editorial">Editorial</option>
              <option value="genre">Género</option>
              <option value="type">Formato</option>
            </select>
          </div>
          <div class="form-group">
            <input OnClick="busca()" type="submit" class="btn btn-outline-primary form-control" value="Buscar"/>
          </div>
        </div>
      </div>
      <br/>
      <div id="encontrado" class="alert alert-info" role="alert">
        @if(count($books) > 1)
          Actualmente hay {{count($books)}} coincidencias
        @elseif(count($books) == 1)
          Actualmente hay {{count($books)}} coincidencia
        @else
          No hay coincidencias
        @endif
      </div>
      <div id="chartContainer" style="height: 300px; width: 100%;"></div>
    </div>
  </div>
</div>
<script>
  function busca()
  {
    filter = $("#searchtype").val();
    value = $("#textsearch").val();//.replace(' ', '+');
    window.location.assign("/"+filter+"/"+value);
  }

  function grafico()
  {
    s1 = $("#gen_0").html();
    s2 = $("#gen_1").html();
    s3 = $("#gen_2").html();
    s4 = $("#gen_").html();

    total = parseInt(s1) + parseInt(s2) + parseInt(s3) + parseInt(s4);

    s1 = parseInt(parseInt(s1)*100/total);
    s2 = parseInt(parseInt(s2)*100/total);
    s3 = parseInt(parseInt(s3)*100/total);
    s4 = parseInt(parseInt(s4)*100/total);

    var chart = new CanvasJS.Chart("chartContainer",
    {
      theme: "light1",
  	  animationEnabled: true,
      legend: {
        maxWidth: 350,
        itemWidth: 120
      },
      data: [
        {
          type: "pie",
          indexLabelFontSize: 12,
          showInLegend: false,
          click: explodePie,
          dataPoints: [
            { y: s1, indexLabel: "Señor ("+s1+"%)" },
            { y: s2, indexLabel: "Señora ("+s2+"%)" },
            { y: s3, indexLabel: "Señore ("+s3+"%)" },
            { y: s4, indexLabel: "Sin asignar ("+s4+"%)" }
          ]
        }
      ]
    });
    chart.render();

    function explodePie(e) {
      autor = ["{{App\Models\Autor::getGenreName(0)}}","{{App\Models\Autor::getGenreName(1)}}","{{App\Models\Autor::getGenreName(2)}}","{{App\Models\Autor::getGenreName(-1)}}"];
      authgenre = e.dataPointIndex;
      location.href = "/authgenre/" + autor[e.dataPointIndex];
      /*console.log(e);
      alert(e.dataPointIndex);*/
    }
  }

  $(document).ready(
    function()
    {
      grafico();
    });
</script>
@endsection
