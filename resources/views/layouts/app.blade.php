<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js" type="text/javascript"></script>

    <!-- Ñapa hasta que sepamos arreglarlo -->
    <!--<link rel="stylesheet" href="{{ asset('3rd_party/font-awesome.min.css') }}">-->
    <link rel="stylesheet" href="{{ asset('3rd_party/fontawesome/css/all.css') }}">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="{{ asset('css/timeline.css') }}" rel="stylesheet">

    <!-- TinyMCE -->
    <script src="https://cdn.tiny.cloud/1/zdqlt2pfx0bk685deqs00y57mp9yqem9tylqubh2cxtd8q4z/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

    <!-- CanvasJS -->
    <script src="{{ asset('js/canvasjs.min.js') }}" defer></script>
    <!--<script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>-->
</head>
<body>
    <div id="app">
        <!--<nav class="navbar navbar-expand-md navbar-dark bg-primary shadow-sm">-->
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
            <!--<div class="container-fluid">-->
            <a class="navbar-brand" href="{{ url('/') }}">{{ config('app.name', 'Laravel') }}</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <!-- Left Side Of Navbar -->
              <ul class="navbar-nav mr-auto">
                @auth
                  <li class="nav-item active">
                    <!-- btn btn-sm btn-outline-light-->
                    <a id="nuevolibro" class="nav-link btn btn-sm btn-outline-light" href="{{ route('acp.newbook') }}">
                      Nuevo libro
                    </a>
                  </li>
                  <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle active" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                      Administración
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="{{ route('acp.autores') }}">Autores</a>
                      <a class="dropdown-item" href="{{ route('acp.datos') }}">Datos</a>
                      <a class="dropdown-item" href="{{ route('acp.libros') }}">Libros</a>
                    </div>
                  </li>
                  <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle active" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                      Préstamos
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="{{ route('prestamos.estado') }}">Estado actual</a>
                      <a class="dropdown-item" href="{{ route('prestamos.gestionar') }}">Gestionar préstamos</a>
                      <a class="dropdown-item" href="{{ route('prestamos.chocoleguis') }}">Amigos</a>
                    </div>
                  </li>
                @endauth
                <li class="nav-item active">
                  <a class="nav-link" href="{{ route('listas.list') }}">
                    Listas
                  </a>
                </li>
              </ul>

              <!-- Right Side Of Navbar -->
              <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                  @if (Route::has('login'))
                  <li class="nav-item">
                    <a class="nav-link active" href="{{ route('login') }}">{{ __('Login') }}</a>
                  </li>
                  @endif
                  @if (Route::has('register'))
                    <li class="nav-item">
                      <a class="nav-link text-white" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                  @endif
                @else
                  <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle active" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                      {{ Auth::user()->name }}
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
                      {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                      @csrf
                    </form>
                  </div>
                </li>
              @endguest
            </ul>
          </div>
            <!--</div>-->
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
