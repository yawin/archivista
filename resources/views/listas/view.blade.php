@extends('layouts.app')

@section('content')
  @auth
    <script src="{{ asset('js/books.js') }}" defer></script>
  @endauth
  <div class="container-fluid">
    <div class="row">
      @auth
        <div class="col-md-9">
      @else
        <div class="col-md-12">
      @endauth
        <div class="card">
          <div class="card-header bg-primary text-white">
            {{$lista->name}}
          </div>
          <div class="card-body">
            <table class="table table-hover">
              <tbody>
                @php
                  $books=0;
                  $datos = ["editorial" => array(), "genaut" => array(), "gen" => array(), "formato" => array(), "tipo" => array(), "pais" => array(), "dias" => array()];
                  //$dat = ["edit" => 0, "genaut" => array(), "gen" => array(), "formato" => "Físico", "tipo" => array(), "pais" => array(), "dias" => 0];
                @endphp
                @foreach($lista->getBooks() as $b)
                  @if($loop->first)
                    <tr>
                      <th class="align-middle">Título</th>
                      <th class="align-middle">Autor</th>
                      <th class="align-middle">Editorial</th>
                      <th class="align-middle">Géneros</th>
                      <th class="align-middle">Tipo de obra</th>
                      <th class="align-middle">Nacionalidad</th>
                      <th class="align-middle">Formato</th>
                      @if($lista->visibleReadDates)
                        <th class="align-middle">Inicio de lectura</th>
                        <th class="align-middle">Fin de lectura</th>
                      @endif
                      @auth <th></th> @endauth
                    </tr>
                  @endif
                  @php
                    $books++;
                    $dat = ["editorial" => 0, "genaut" => array(), "gen" => array(), "formato" => "Físico", "tipo" => array(), "pais" => array(), "dias" => 0];
                  @endphp
                  <tr>
                    <td class="align-middle">{{ $b->title }}</td>
                    <td>@php $dat['genaut']["total"] = 0; @endphp
                      @foreach($b->getAuthors() as $a)
                        <a href="{{ route('filtered',['author',$a[1]])}}">{{$a[1]}} {!! App\Models\Autor::getGenreSymbol($a[2]) !!}</a>@if(!$loop->last),@endif
                        @php if(isset($dat['genaut'][$a[2]])){ $dat['genaut'][$a[2]]++; }
                             else{ $dat['genaut'][$a[2]] = 1; }
                             $dat['genaut']["total"]++;
                        @endphp
                      @endforeach
                    </td>
                    <td class="align-middle">
                      @php $dat['editorial'] = App\Models\Editorial::getName($b->editorial); @endphp
                      <a href="{{ route('filtered',['editorial',App\Models\Editorial::getName($b->editorial)])}}">{{ App\Models\Editorial::getName($b->editorial) }}</a>
                    </td>
                    <td class="align-middle">@php $dat['gen']["total"] = 0; $dat['tipo']["total"] = 0; @endphp
                      @foreach($b->getGenres() as $g)
                        @php $celda = 'gen';
                             if(App\Models\Genre::esFormato($g[0]))
                               $celda = 'tipo';

                             if(isset($dat[$celda][$g[1]])){ $dat[$celda][$g[1]]++; }
                             else{ $dat[$celda][$g[1]] = 1; }
                             $dat[$celda]["total"]++;
                        @endphp
                      @endforeach
                      @foreach($dat['gen'] as $k => $v)
                        @if($k != 'total') <a href="{{ route('filtered',['genre',$k])}}">{{$k}}</a>@if(!$loop->last),@endif @endif
                      @endforeach
                    </td>
                    <td>
                      @foreach($dat['tipo'] as $k => $v)
                        @if($k != 'total') <a href="{{ route('filtered',['genre',$k])}}">{{$k}}</a>@if(!$loop->last),@endif @endif
                      @endforeach
                    </td>
                    @php
                        $c = array();
                        foreach($b->getAuthors() as $a)
                        {
                          if(!empty($a[3])){array_push($c, $a[3]);}
                        }
                        $c = array_unique($c);
                        $dat['pais']["total"] = 0;
                    @endphp
                    <td class="align-middle">
                      @foreach($c as $p)
                        {{$paises[$p]}}@if(!$loop->last),@endif
                        @php if(isset($dat['pais'][$paises[$p]])){ $dat['pais'][$paises[$p]]++; }
                             else{ $dat['pais'][$paises[$p]] = 1; }
                             $dat['pais']["total"]++;
                        @endphp
                      @endforeach
                    </td>
                    @php
                      $f = "Físico";
                      foreach($b->getTypes() as $t){ if($t[1] == "Digital"){$f = "Digital";} }
                      $dat["formato"] = $f;
                    @endphp
                    <td class="align-middle">{{$f}}</td>
                    @if($lista->visibleReadDates)
                      <td class="align-middle">{{ $b->start }}</td>
                      <td class="align-middle">{{ $b->end }}</td>
                      @php
                        $sts = strtotime($b->start);
                        $ets = strtotime($b->end);
                        $timeDiff = abs($ets - $sts);
                        $numberDays = $timeDiff/86400;  // 86400 seconds in one day
                        $dat["dias"] = 1 + intval($numberDays);
                      @endphp
                    @endif
                    @auth
                      <td style="width:100px;" class="align-middle"><button OnClick="eliminaLibroDeLista({{ $b->id }});" class="btn btn-sm btn-danger" title="Eliminar"><span class="fas fa-times"></span></button></td>
                    @endauth
                  </tr>
                  @php
                    if(isset($datos["editorial"][$dat["editorial"]])){$datos["editorial"][$dat["editorial"]]++;}
                    else{$datos["editorial"][$dat["editorial"]] = 1;}

                    foreach($dat["genaut"] as $g => $v)
                    {
                      if(!is_string($g) && $g != -1)
                      {
                        $gen = App\Models\Autor::getGenreName($g);
                        if(isset($datos["genaut"][$gen]))
                        {
                          $datos["genaut"][$gen] += ($v/$dat["genaut"]["total"]);
                        }
                        else
                        {
                          $datos["genaut"][$gen] = ($v/$dat["genaut"]["total"]);
                        }
                      }
                    }

                    foreach($dat["gen"] as $g => $v)
                    {
                      if($g != "total")
                      {
                        if(isset($datos["gen"][$g]))
                        {
                          $datos["gen"][$g] += ($v/$dat["gen"]["total"]);
                        }
                        else
                        {
                          $datos["gen"][$g] = ($v/$dat["gen"]["total"]);
                        }
                      }
                    }

                    foreach($dat["tipo"] as $g => $v)
                    {
                      if($g != "total")
                      {
                        if(isset($datos["tipo"][$g]))
                        {
                          $datos["tipo"][$g] += ($v/$dat["tipo"]["total"]);
                        }
                        else
                        {
                          $datos["tipo"][$g] = ($v/$dat["tipo"]["total"]);
                        }
                      }
                    }

                    if(isset($datos["formato"][$dat["formato"]])){$datos["formato"][$dat["formato"]]++;}
                    else{$datos["formato"][$dat["formato"]] = 1;}

                    foreach($dat["pais"] as $g => $v)
                    {
                      if($g != "total")
                      {
                        if(isset($datos["pais"][$g]))
                        {
                          $datos["pais"][$g] += ($v/$dat["pais"]["total"]);
                        }
                        else
                        {
                          $datos["pais"][$g] = ($v/$dat["pais"]["total"]);
                        }
                      }
                    }

                    if(isset($datos["dias"][$dat["dias"]])){$datos["dias"][$dat["dias"]]++;}
                    else{$datos["dias"][$dat["dias"]] = 1;}
                  @endphp
                @endforeach
                @if($books == 0)
                  <tr><td>No hay libros en la lista</td></tr>
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
      @auth
        <div class="col-md-3">
          <div class="card">
            <div class="card-header bg-primary text-white">Añadir libro</div>
            <div class="card-body">
              <form id="editor" method="POST" action="{{ route('listas.putbookinlista') }}">
                @csrf
                @method('PUT')
                <input id="lista_id" name="lista_id" hidden value="{{$lista->id}}"/>
                <input id="rm_book" name="rm_book" hidden value="-1"/>
                <input id="lista_action" name="lista_action" hidden value="add"/>
                <div class="form-group">
                  <select id="book_id" name="book_id" class="form-control">
                    <option disabled selected value> Elegir un libro </option>
                    @foreach(App\Models\Book::getNotInLista($lista->id) as $b)
                      <option value="{{$b->id}}">{{ $b->title }}</option>
                    @endforeach
                  </select>
                </div>
                <div @if(!$lista->visibleReadDates) hidden @endif>
                  {!! App\Models\UIAssistant::getDateField("Inicio de lectura", "start", date("Y-m-d")) !!}
                  {!! App\Models\UIAssistant::getDateField("Fin de lectura", "end", date("Y-m-d")) !!}
                </div>
                <div class="form-group">
                  <input type="submit" class="btn btn-outline-primary form-control" value="Guardar"/>
                </div>
              </form>
            </div>
          </div>
        </div>
      @endauth
    </div>
    <div class="row"><div class="col-md-12"><hr/></div></div>
    <div class="row">
      <div class="col-md-3" style="margin-bottom:5px;"><h4>Editoriales</h4><div id="editorial" style="height: 150px; width: 100%;"></div></div>
      <div class="col-md-3" style="margin-bottom:5px;"><h4>Tipo de obra</h4><div id="tipo" style="height: 150px; width: 100%;"></div></div>
      <div class="col-md-3" style="margin-bottom:5px;"><h4>Géneros</h4><div id="gen" style="height: 150px; width: 100%;"></div></div>
      <div class="col-md-3" style="margin-bottom:5px;"><h4>Género de los autores</h4><div id="genaut" style="height: 150px; width: 100%;"></div></div>
      <div class="col-md-3" style="margin-bottom:5px;"><h4>Formato</h4><div id="formato" style="height: 150px; width: 100%;"></div></div>
      <div class="col-md-3" style="margin-bottom:5px;"><h4>Nacionalidad</h4><div id="pais" style="height: 150px; width: 100%;"></div></div>
      <div class="col-md-3" style="margin-bottom:5px;" @if(!$lista->visibleReadDates) hidden @endif><h4>Días de lectura</h4><div id="dias" style="height: 150px; width: 100%;"></div></div>
      <script>
        function grafico(dat, field)
        {
          datos = JSON.parse(dat);
          data = [];
          for(var k in datos)
          {
            data.push({y:datos[k], indexLabel:k});
          }

          var chart = new CanvasJS.Chart(field,
          {
            theme: "light1",
        	  animationEnabled: true,
            legend: {
              maxWidth: 350,
              itemWidth: 120
            },
            data: [
              {
                type: "pie",
            		indexLabelFontSize: 12,
                showInLegend: false,
                dataPoints: data
              }
            ]
          });
          chart.render();
        }
      </script>
      @foreach($datos as $k => $v)
        <script>
          $(document).ready(
            function()
            {
              grafico('{!! json_encode($v) !!}', "{{$k}}");
            });
        </script>
      @endforeach
    </div>
  </div>
  @endsection
