<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if(!Archivista\Environment::getValue("APP_CONFIGURED"))
{
  Route::get('/', [App\Http\Controllers\SetupController::class, 'setup'])->name("index");
  Route::get('/setup', [App\Http\Controllers\SetupController::class, 'setup'])->name("setup");
  Route::put('/setup', [App\Http\Controllers\SetupController::class, 'save_setup'])->name("save_setup");
  Route::get('/setup/migrate', [App\Http\Controllers\SetupController::class, 'migrate'])->name("migrate");
  Route::get('/setup/requirements', [App\Http\Controllers\SetupController::class, 'requirements'])->name("requirements");
  Route::get('/home', [App\Http\Controllers\SetupController::class, 'endsetup'])->name("home");
  Auth::routes(['verify' => false, 'login' => false]);
}
else
{
  Route::get('/', [App\Http\Controllers\GuestController::class, 'index'])->name("index");

  Auth::routes(["register" => false]);

  Route::get('/home', [App\Http\Controllers\GuestController::class, 'home'])->name('home');
  Route::get("/api/book/",[App\Http\Controllers\GuestApi::class,'apicall_book'])->name("api.book");
  Route::get("/api/book/{isbn}",[App\Http\Controllers\GuestApi::class,'apicall_book']);
  Route::get("/api/isbn/{isbn}",[App\Http\Controllers\GuestApi::class,'apicall_isbn'])->name('api.isbn');
  Route::get("/api/isbn/{isbn}/{method}",[App\Http\Controllers\GuestApi::class,'apicall_isbn']);

  Route::get("/api/acp/addAuth/{bookid}/{authid}",[App\Http\Controllers\AcpApi::class,'addAuth'])->name("api.acp.addauth");
  Route::get("/api/acp/delAuth/{bookid}/{authid}",[App\Http\Controllers\AcpApi::class,'delAuth'])->name("api.acp.delauth");
  Route::get("/api/acp/addGen/{bookid}/{genreid}",[App\Http\Controllers\AcpApi::class,'addGen'])->name("api.acp.addgen");
  Route::get("/api/acp/delGen/{bookid}/{genreid}",[App\Http\Controllers\AcpApi::class,'delGen'])->name("api.acp.delgen");
  Route::get("/api/acp/addType/{bookid}/{typeid}",[App\Http\Controllers\AcpApi::class,'addType'])->name("api.acp.addtype");
  Route::get("/api/acp/delType/{bookid}/{typeid}",[App\Http\Controllers\AcpApi::class,'delType'])->name("api.acp.deltype");
  Route::get("/api/acp/updateField/{bookid}/{field}/{value}",[App\Http\Controllers\AcpApi::class,'putUpdateBook'])->name("api.acp.updatefield");
  Route::get("/api/acp/delPrest/{id}",[App\Http\Controllers\AcpApi::class,'delPrest'])->name("api.acp.delPrest");

  Route::get('/newBook', [App\Http\Controllers\HomeController::class, 'newBook'])->name('acp.newbook');
  Route::put('/newBook', [App\Http\Controllers\HomeController::class, 'insertBook']);
  Route::get('/book/{id}', [App\Http\Controllers\HomeController::class, 'viewBook'])->name('acp.view');

  Route::get('/editLibro/{id}',[App\Http\Controllers\HomeController::class, 'editLibro'])->name('acp.editLibro');
  Route::put('/editLibro', [App\Http\Controllers\HomeController::class, 'putEditLibro'])->name('acp.editbook');

  Route::get('/libros',[App\Http\Controllers\HomeController::class, 'libros'])->name('acp.libros');
  Route::get('/datos',[App\Http\Controllers\HomeController::class, 'datos'])->name('acp.datos');
  Route::put('/datos',[App\Http\Controllers\HomeController::class, 'putdatos'])->name('acp.putdatos');
  Route::get('/autores',[App\Http\Controllers\HomeController::class, 'autores'])->name('acp.autores');
  Route::put('/autores',[App\Http\Controllers\HomeController::class, 'putautores'])->name('acp.putautores');
  Route::put('/unificautores',[App\Http\Controllers\HomeController::class, 'unificautores'])->name('acp.unificautores');

  Route::get('/estado',[App\Http\Controllers\PrestamosController::class, 'estado'])->name('prestamos.estado');
  Route::get('/amigos',[App\Http\Controllers\PrestamosController::class, 'chocoleguis'])->name('prestamos.chocoleguis');
  Route::put('/amigos',[App\Http\Controllers\PrestamosController::class, 'putchocoleguis'])->name('prestamos.putchocoleguis');
  Route::get('/gestionar',[App\Http\Controllers\PrestamosController::class, 'gestionar'])->name('prestamos.gestionar');
  Route::put('/gestionar',[App\Http\Controllers\PrestamosController::class, 'putgestionar'])->name('prestamos.putgestionar');

  Route::get('/listas',[App\Http\Controllers\ListasController::class, 'listas'])->name('listas.list');
  Route::get('/listas/{id}',[App\Http\Controllers\ListasController::class, 'view'])->name('listas.view');
  Route::put('/listas',[App\Http\Controllers\ListasController::class, 'putlistas'])->name('listas.putlistas');
  Route::put('/modifylist',[App\Http\Controllers\ListasController::class, 'putbookinlista'])->name('listas.putbookinlista');

  Route::get('/{filter}/{value}', [App\Http\Controllers\GuestController::class, 'indexFiltered'])->name('filtered');
}
