<?php

namespace App\Http\Controllers;

use Monarobase\CountryList\CountryListFacade;
use Illuminate\Http\Request;
use App;


class HomeController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function newBook()
    {
      return view('acp.newbook');
    }

    public function viewBook($id)
    {
      $book = App\Models\Book::find($id);
      return view('acp.insert', compact('book'));
    }

    public function insertBook(Request $request)
    {
      $request->validate([
        'title' => 'required',
        'author' => 'required',
        'editorial' => 'required',
        'isbn' => 'required',
        'genero' => 'required',
        'date' => 'required',
        'pagecount' => 'required',
        'tipo' => 'required'
      ]);

      $book = new App\Models\Book;
      $book->title = $request->title;

      if(!empty($request->descript))
        $book->descript = $request->descript;

      $book->editorial = $request->editorial;
      $book->isbn = $request->isbn;
      $book->setDate($request->date);
      $book->pagecount = $request->pagecount;
      $book->save();

      foreach(explode(", ", $request->author) as $a)
      {
        $book->addAuthor(App\Models\Autor::getIdOf($a));
      }

      foreach($request->genero as $g)
      {
        $book->addGenre($g);
      }

      foreach($request->tipo as $t)
      {
        $book->addType($t);
      }

      return redirect()->route("acp.newbook");
    }

    public function datos()
    {
      return view('acp.datos');
    }

    public function putdatos(Request $request)
    {
      $request->validate([
        'edit_id' => 'required',
        'edit_field' => 'required'
      ]);

      if($request->edit_field != "tipo_obra")
      {
        switch($request->edit_field)
        {
          case "e":
            $data = App\Models\Editorial::getIfExist($request->edit_id);
            break;

          case "g":
            $data = App\Models\Genre::getIfExist($request->edit_id);
            break;

          case "t":
            $data = App\Models\Tipo::getIfExist($request->edit_id);
            break;

        }

        if($request->edit_name != "")
        {
          $data->name = $request->edit_name;
          $data->save();
        }
        else
        {
          $data->elimina();
        }
      }
      else
      {
        $data = App\Models\TipoObra::getIfExist($request->edit_id);

        if($request->edit_id == -1)
        {
          $data->genre_id = $request->tipo;
          $data->save();
        }
        else
        {
          $data->delete();
        }
      }

      return redirect()->route("acp.datos");
    }

    public function autores()
    {
      $paises = CountryListFacade::getList("es");
      return view('acp.autores', compact("paises"));
    }

    public function putautores(Request $request)
    {
      $request->validate([
        'autor_id' => 'required',
        'genre' => 'required'
      ]);

      $autor = App\Models\Autor::getIfExist($request->autor_id);

      if($request->name != "")
      {
        $autor->name = $request->name;
        $autor->genre = $request->genre;
        $autor->country = (isset($request->country)) ? $request->country : "";
        $autor->save();
      }
      else
      {
        $autor->delete();
      }

      return redirect()->route("acp.autores");
    }

    public function libros()
    {
      return view('acp.libros');
    }

    public function editLibro($id)
    {
      $book = App\Models\Book::find($id);
      return view('acp.editlibro', compact('book'));
    }

    public function putEditLibro(Request $request)
    {
      $request->validate([
        'id' => 'required',
        'title' => 'required',
        'isbn' => 'required',
        'date' => 'required',
        'pagecount' => 'required'
      ]);

      $book = App\Models\Book::find($request->id);

      if($request->isbn != -1)
      {
        $request->validate([
          'author' => 'required',
          'editorial' => 'required',
          'genero' => 'required',
          'tipo' => 'required'
        ]);

        $book->title = $request->title;

        if(!empty($request->descript))
          $book->descript = $request->descript;

        $book->editorial = $request->editorial;
        $book->isbn = $request->isbn;
        $book->setDate($request->date);
        $book->pagecount = $request->pagecount;

        $book->save();

        foreach($request->author as $a)
        {
          $book->addAuthor($a);
        }

        foreach($request->genero as $g)
        {
          $book->addGenre($g);
        }

        foreach($request->tipo as $t)
        {
          $book->addType($t);
        }
      }
      else
      {
        $book->elimina();
      }

      return redirect()->route("acp.libros");
    }

    public function unificautores(Request $request)
    {
      $request->validate([
        'unify-input' => 'required'
      ]);

      $autores = explode(",", $request['unify-input']);
      if(count($autores) > 1)
      {
        for($i = 1; $i < count($autores); $i++)
        {
          App\Models\Book::changeAuthor($autores[$i], $autores[0]);
          App\Models\Autor::find($autores[$i])->delete();
        }
      }

      return redirect()->route("acp.autores");
    }
}
