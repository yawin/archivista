<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bookauthor extends Model
{
    use HasFactory;
}

class Bookgenre extends Model
{
    use HasFactory;
}

class Booktype extends Model
{
    use HasFactory;
}

class Book extends Model
{
    use HasFactory;

    public function setDate($date)
    {
      $d = strtotime($date);
      $this->date = date('Y-m-d', $d);
    }

    public function elimina()
    {
      foreach(Bookauthor::where('bookid', '=', $this->id)->get() as $ba)
      {
        $ba->delete();
      }
      foreach(Bookgenre::where('bookid', '=', $this->id)->get() as $bg)
      {
        $bg->delete();
      }
      foreach(Booktype::where('bookid', '=', $this->id)->get() as $bt)
      {
        $bt->delete();
      }

      foreach(Listabook::where('bookid', '=', $this->id)->get() as $lb)
      {
        $lb->delete();
      }

      $this->delete();
    }

    public static function getAll()
    {
      return Book::orderBy('title')->get();
    }

    public static function getFromTitle($title)
    {
      $query = Book::orderBy('title');
      foreach($title as $t)
      {
        $query = $query->orWhere('title', 'like', '%'.$t.'%');
      }

      return $query->orderBy('books.title')->distinct()->get();
    }

    public static function getFromAuthor($author)
    {
      $query = Book::join('bookauthors', 'books.id', '=', 'bookauthors.bookid')->join('autors', 'bookauthors.authorid', '=', 'autors.id')->select('books.*');
      foreach($author as $a)
      {
        $query = $query->orWhere('autors.name', 'like', '%'.$a.'%');
      }

      return $query->orderBy('books.title')->distinct()->get();
    }

    public static function getFromAuthorGenre($authorgenre)
    {
      $query = Book::join('bookauthors', 'books.id', '=', 'bookauthors.bookid')->join('autors', 'bookauthors.authorid', '=', 'autors.id')->select('books.*');
      foreach($authorgenre as $a)
      {
        $query = $query->orWhere('autors.genre', 'like', '%'.Autor::getGenreId($a).'%');
      }

      return $query->orderBy('books.title')->distinct()->get();
    }

    public static function getFromEditorial($editorial)
    {
      $query = Book::join('editorials', 'books.editorial', '=', 'editorials.id')->select('books.*');

      foreach($editorial as $e)
      {
        $query = $query->orWhere("editorials.name", "like", '%'.$e.'%');
      }

      return $query->orderBy('books.title')->distinct()->get();
    }

    public static function getFromGenre($genre)
    {
      $query = Book::join('bookgenres', 'books.id', '=', 'bookgenres.bookid')->join('genres', 'bookgenres.genreid', '=', 'genres.id')->select('books.*');
      foreach($genre as $g)
      {
        $query = $query->orWhere('genres.name', 'like', '%'.$g.'%');
      }
      return $query->orderBy('books.title')->distinct()->get();
    }

    public static function getFromType($type)
    {
      $query = Book::join('booktypes', 'books.id', '=', 'booktypes.bookid')->select('books.*')->join('tipos', 'booktypes.typeid', '=', 'tipos.id');
      foreach($type as $t)
      {
        $query = $query->orWhere('tipos.name', 'like', '%'.$t.'%');
      }
      return $query->orderBy('books.title')->distinct()->get();
    }

    public function addAuthor($authorid)
    {
      if(empty(Bookauthor::where("authorid", "=", $authorid)->where("bookid", "=", $this->id)->first()))
      {
        $ba = new Bookauthor();
        $ba->bookid = $this->id;
        $ba->authorid = $authorid;
        $ba->save();
      }
    }
    public function delAuthor($authorid)
    {
      $ba = Bookauthor::where("authorid", "=", $authorid)->where("bookid", "=", $this->id)->first();

      if(!empty($ba))
        $ba->delete();
    }

    public function addGenre($genreid)
    {
      if(empty(Bookgenre::where("genreid", "=", $genreid)->where("bookid", "=", $this->id)->first()))
      {
        $bg = new Bookgenre();
        $bg->bookid = $this->id;
        $bg->genreid = $genreid;
        $bg->save();
      }
    }
    public function delGenre($genreid)
    {
      $ba = Bookgenre::where("genreid", "=", $genreid)->where("bookid", "=", $this->id)->first();

      if(!empty($ba))
        $ba->delete();
    }

    public function addType($typeid)
    {
      if(empty(Booktype::where("typeid", "=", $typeid)->where("bookid", "=", $this->id)->first()))
      {
        $bt = new Booktype();
        $bt->bookid = $this->id;
        $bt->typeid = $typeid;
        $bt->save();
      }
    }
    public function delType($typeid)
    {
      $ba = Booktype::where("typeid", "=", $typeid)->where("bookid", "=", $this->id)->first();

      if(!empty($ba))
        $ba->delete();
    }

    public function getAuthors()
    {
      $autores = Autor::join('bookauthors', 'autors.id', '=', 'bookauthors.authorid')->select('autors.id as id','autors.name as name','autors.genre as genre','autors.country as country')->where("bookauthors.bookid", "=", $this->id)->orderBy("autors.name")->get();

      $ret = array();
      foreach($autores as $a)
      {
        array_push($ret, [$a->id,$a->name,$a->genre,$a->country]);
      }

      return $ret;
    }
    public function getAllAuthorId()
    {
      $ret = array();
      foreach(Bookauthor::where('bookid', '=', $this->id)->get() as $ba)
      {
        $a = Autor::find($ba->authorid);
        array_push($ret, $ba->authorid);
      }

      return $ret;
    }

    public function getGenres()
    {
      $genres = Genre::join('bookgenres', 'genres.id', '=', 'bookgenres.genreid')->select('genres.id as id','genres.name as name')->where("bookgenres.bookid", "=", $this->id)->orderBy("genres.name")->get();

      $ret = array();
      foreach($genres as $g)
      {
        array_push($ret, [$g->id,$g->name]);
      }

      return $ret;
    }
    public function getAllGenreId()
    {
      $ret = array();
      foreach(Bookgenre::where('bookid', '=', $this->id)->get() as $bg)
      {
        array_push($ret, $bg->genreid);
      }

      return $ret;
    }

    public function getTypes()
    {
      $types = Tipo::join('booktypes', 'tipos.id', '=', 'booktypes.typeid')->select('tipos.id as id','tipos.name as name')->where("booktypes.bookid", "=", $this->id)->orderBy("tipos.name")->get();

      $ret = array();
      foreach($types as $t)
      {
        array_push($ret, [$t->id,$t->name]);
      }

      return $ret;
    }
    public function getAllTypeId()
    {
      $ret = array();
      foreach(Booktype::where('bookid', '=', $this->id)->get() as $bt)
      {
        array_push($ret, $bt->typeid);
      }

      return $ret;
    }

    public static function changeAuthor($from, $to)
    {
      foreach(Bookauthor::where('authorid', '=', $from)->get() as $ba)
      {
        $ba->authorid = $to;
        $ba->save();
      }
    }

    public static function getNotPrestados()
    {
      $lib = Prestamo::join("books", "prestamos.libro", "=", "books.id")->select("books.id")->get()->toArray();
      return Book::whereNotIn("id", $lib)->orderBy("title")->get();
    }

    public static function getNotInLista($listaid)
    {
      $lib = Listabook::select("listabooks.bookid as id")->where("listaid", "=", $listaid)->get()->toArray();
      return Book::whereNotIn("id", $lib)->orderBy("title")->get();
    }
}
