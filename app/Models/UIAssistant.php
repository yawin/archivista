<?php

namespace App\Models;

class UIAssistant
{
  public static function formatArray($data, $fields)
  {
    $ret = array();
    foreach($data as $dat)
    {
      $f = array();
      foreach($fields as $field)
      {
        array_push($f, $dat->$field);
      }

      array_push($ret, $f);
    }

    return $ret;
  }

  public static function getInputField($label, $name, $value, $placeholder = "")
  {
    return "
      <div class='form-group row'>
        <label for='$name' class='col-md-4 col-form-label text-md-right'>$label</label>
        <div class='col-md-8'>
          <input id='$name' type='text' class='form-control' name='$name' value='$value' placeholder='$placeholder' required>
        </div>
      </div>";
  }
  public static function getPasswordField($label, $name, $value, $placeholder = "")
  {
    return "
      <div class='form-group row'>
        <label for='$name' class='col-md-4 col-form-label text-md-right'>$label</label>
        <div class='col-md-8'>
          <input id='$name' type='password' class='form-control' name='$name' value='$value' placeholder='$placeholder' required>
        </div>
      </div>";
  }
  public static function getDateField($label, $name, $value, $placeholder = "")
  {
    return "
    <div class='form-group row'>
      <label for='$name' class='col-md-5 col-form-label text-md-right'>$label</label>
      <div class='col-md-7'>
        <input id='$name' type='date' class='form-control' name='$name' value='$value' placeholder='$placeholder' required>
      </div>
    </div>";
  }

  public static function getTextField($label, $name, $value, $width = "100%", $height = "100%", $req = "required")
  {
    return "
      <div class='form-group row'>
        <label for='$name' class='col-md-4 col-form-label text-md-right'>$label</label>
        <div class='col-md-8'>
          <textarea id='$name' type='text' class='form-control' name='$name' style='width:$width; height:$height;' $req>$value</textarea>
        </div>
      </div>";
  }

  public static function getTextField2md($label, $name, $value, $width = "100%", $height = "100%", $req = "required")
  {
    return "
      <div class='form-group row'>
        <label for='$name' class='col-md-2 col-form-label text-md-right'>$label</label>
        <div class='col-md-10'>
          <textarea id='$name' type='text' class='form-control' name='$name' style='width:$width; height:$height;' $req>$value</textarea>
        </div>
      </div>";
  }

  public static function getSelectField($label, $name, $values, $height = "100%", $select="-1")
  {
    $options = "<option value='-1' selected disabled>Selecciona $name</option>";
    foreach($values as $v)
    {
      $options = $options."<option value='$v[0]'";
      if($v[0] == $select)
        $options = $options." selected ";

      $options = $options.">$v[1]</option>";
    }

    if($label != "")
    {
      return "
        <div class='form-group row'>
          <label for='$name' class='col-md-4 col-form-label text-md-right'>$label</label>
          <div class='col-md-8'>
            <select id='select_$name' name='$name' class='form-control select' style='width:100%; height:$height;'>
              $options
            </select>
          </div>
        </div>";
    }
    else
    {
      return "
        <select id='select_$name' name='$name' class='form-control select' style='width:100%; height:$height;'>
          $options
        </select>";
    }
  }

  public static function getMultipleSelectField($label, $name, $values, $height = "100%", $select = [])
  {
    $options = "";
    foreach($values as $v)
    {
      $options = $options."<option value='$v[0]'";
      if(in_array($v[0], $select))
        $options = $options." selected ";

      $options = $options.">$v[1]</option>";
    }

    return "
      <div class='form-group row' style='height:$height;'>
        <label for='$name' class='col-md-4 col-form-label text-md-right'>$label</label>
        <div class='col-md-8'>
          <select multiple id='select_$name' name='$name".'[]'."' class='form-control select' style='width:100%; height:100%;'>
            $options
          </select>
        </div>
      </div>";
  }
}
