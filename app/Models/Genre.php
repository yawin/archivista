<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    use HasFactory;
    public static function esFormato($gen)
    {
      return !empty(TipoObra::where('genre_id', '=', $gen)->first());
    }

    public function elimina()
    {
      foreach(Book::getFromGenre([$this->name]) as $b)
      {
        $b->delGenre($this->id);
      }

      $to = TipoObra::where("genre_id", "=", $this->id);
      if(!empty($to))
        $to->delete();

      $this->delete();
    }

    public static function getAll()
    {
      return Genre::orderBy('name')->get();
    }

    public static function getId($name)
    {
      return Genre::where('name', '=', $name)->first()->id;
    }

    public static function getIfExist(string $id)
    {
      $tmp = Genre::find($id);
      if(empty($tmp))
      {
        return new Genre;
      }
      return $tmp;
    }
}
