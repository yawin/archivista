<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lista extends Model
{
    use HasFactory;

    public static function formatDate($date)
    {
      $d = strtotime($date);
      return date('Y-m-d', $d);
    }

    public static function getAll()
    {
      return Lista::orderBy('name')->get();
    }

    public function getBooks()
    {
      return Book::join('listabooks', 'books.id', '=', 'listabooks.bookid')->select("books.id as id", "books.title as title", "books.editorial as editorial", "listabooks.date_start as start", "listabooks.date_end as end")->where("listabooks.listaid", "=", $this->id)->orderBy('listabooks.date_start')->get();
    }

    public function addBook($bookid, $start, $end)
    {
      $lb = new Listabook;
      $lb->listaid = $this->id;
      $lb->bookid = $bookid;
      $lb->date_start = self::formatDate($start);
      $lb->date_end = self::formatDate($end);
      $lb->save();
    }

    public function rmBook($bookid)
    {
      $lb = Listabook::where("bookid", "=", $bookid)->first();
      if(!empty($lb)){$lb->delete();}
    }

    public function elimina()
    {
      foreach(Listabook::where('listaid', '=', $this->id)->get() as $lb)
      {
        $lb->delete();
      }

      $this->delete();
    }
}
